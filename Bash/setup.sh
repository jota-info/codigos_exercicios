#!/bin/bash

echo "Digite seu usuário: ";
read userName;

sudo apt update;
sudo apt upgrade -y;

sudo apt install -y apache2;
sudo apt install -y composer;
sudo apt install -y php7.0;
sudo apt install -y php7.0-*;
sudo apt install -y mysql-server;
sudo apt install -y mysql-client;
sudo apt install -y phpmyadmin;
sudo apt install -y nodejs;
sudo apt install -y npm;

sudo adduser $userName www-data;
mkdir ~/html;
ln -s /var/www/html ~/html;

sudo chown -R $userName:www-data /var/www/html;
sudo chmod -R 775 /var/www/html;

sudo ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf;
sudo a2enconf phpmyadmin.conf;
sudo systemctl restart apache2.service;

sudo apt install -y git;
echo "Digite seu nome de usuário GIT: ";
read gitName;
echo "Digite seu email de usuário GIT: ";
read gitEmail;
git config --global user.name "$gitName";
git config --global user.email "$gitEmail";

sudo add-apt-repository ppa:webupd8team/atom;
sudo apt update; sudo apt install atom;
sudo apt update; sudo apt install atom;


echo "Qual interface gráfica instalar?";
echo "GNOME -> Digite 1";
echo "Unity -> Digite 2";
echo "XFCE -> Digite 3";
echo "LXDE -> Digite 4";
read enviroment;

if [ "$enviroment" = "1" ]
then
    echo "Instalando Gnome!";
    sudo apt install -y ubuntu-gnome-desktop;
elif [ "$enviroment" = "2" ]
then
    echo "Instalando Unity!";
    sudo apt install -y ubuntu-desktop;
elif [ "$enviroment" = "3" ]
then
    echo "Instalando XFCE!";
    sudo apt install -y xubuntu-desktop;
elif [ "$enviroment" = "4" ]
then
    echo "Instalando LXDE!";
    sudo apt install -y lubuntu-desktop;
fi

sudo reboot;